U.S. Ski and Snowboard Java Test
--------------------------------

## Goal

Using MySQL, Maven, and Spring MVC, build a web portal to display the member data contained in data.xlsx.


## Requirements

* Use the data contained in data.xlsx as test data for building the application.  The SQL tables you create should have the same name as each of the sheets.  The table columns should have the same name as each of the sheet headers.
* Create a standalone web server that returns a list of members.  Each member entry should contain the same information displayed in the image below.  Only address information from the primary address (PRIMARY == 'Y') should be displayed on the main page.

![Member Data](images/Member.png)

* A link should be placed on each member entry that links to another page that allows for:
    * Creation of a new address
    * Deletion of any existing addresses
    * Editting of any existing address


## Submission

Please email a copy of your solution.
